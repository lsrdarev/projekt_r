#!/bin/python3
import json
import ipaddress
import time
import datetime
import sys
import collections
import statistics
import signal
import platform
import subprocess
import os

#---------------------------------------------------------------------------
landscape = json.load(open('network_landscape.json',"r"))
firewall  = json.load(open('firewall_configuration.json',"r"))
computers = []
infected_computers = {}  #key=zarazeno racunalo(ip), value=lista [s kojeg racunala se prosirio crv, kada je zarazeno]
all_possible_hosts = []

datacenter_computers = []
WAN_computers = []
distribution_computers = []
DMZ_computers = []

propagation_wave_counter = 1
killswitch_URL = "www.iuqerfsodp9ifjaposdfjhgosurijfaewrwergwea.com"
killswitch_domain_registered = False   #www.iuqerfsodp9ifjaposdfjhgosurijfaewrwergwea.com
check_real_domain = False
start_time = 0
end_time = 0
ksw_time = None
ksw_ip = None
#---------------------------------------------------------------------------



def SIGQUIT_handler(sigNum, frame):
    #stops the propagation
    global killswitch_domain_registered
    killswitch_domain_registered = True
    print("\nkillswitch domain registered at ", datetime.datetime.now())
    time.sleep(4)
    return

def ping(host):
    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    return subprocess.call(command) == 0


def initialize_global_variables():

    #all_possible_hosts
    for interface in landscape["router"]["interfaces"]:
        #zastavicom forsira stvaranje network objekta, postavlja bitove krajnje desno(ovisno o maski) na 0
        network  = ipaddress.ip_network(interface, strict=False)
        # all_possible_hosts.extend(network.hosts()) //nisu stringovi
        for host in network.hosts():
            all_possible_hosts.append(str(host))
    
    #razvrstava adrese po suceljima
    for computer in landscape["computers"]:
        ip = computer["ip_address"]
        if("-" in ip):
            computers.extend(get_list_from_ip_range(ip))
        else:
            computers.append(ip)

        if("192.168.52" in ip):
            datacenter_computers.append(ip)

        elif("192.168.53" in ip):
            if("-" in ip):
                distribution_computers.extend(get_list_from_ip_range(ip))
            else:
                distribution_computers.append(ip)

        elif("192.168.54" in ip):
            if("-" in ip):
                WAN_computers.extend(get_list_from_ip_range(ip))
            else:
                WAN_computers.append(ip)

        elif("203.0.113" in ip):
            DMZ_computers.append(ip)

        else:
            print("unrecognzed ip")


def is_ip_in_interface(ip,interface):

    ret_val = False
    network  = ipaddress.ip_network(interface, strict=False)
    for host in network.hosts():
        if(str(host) == ip):
            ret_val = True
    
    return ret_val


def reset_global_variables():

    global infected_computers,killswitch_domain_registered,propagation_wave_counter,start_time,end_time,ksw_time,ksw_ip,check_real_domain

    infected_computers.clear()
    killswitch_domain_registered = False
    propagation_wave_counter = 1
    start_time = 0
    end_time = 0
    ksw_time = None
    ksw_ip = None
    check_real_domain = False



def print_landscape_info():
    for computer in landscape["computers"]:
        print("----------------------------------------------------------")
        for x in computer:
            print(x,": ", computer[x] if(computer[x]!='' and computer[x]!=['']) else "unknown")
    
    input("\n\nPress Enter to continue...")
    print("\n-------------------------------------")


def print_bank_info():
    for x in distribution_computers:
        print("distribution -----> ",x)
    print("-------------------------------------")
    for x in datacenter_computers:
        print("datacenter -----> ",x)
    print("---------------------------------")
    for x in WAN_computers:
        print("WAN -----> ",x)
    print("------------------------------")
    for x in DMZ_computers:
        print("DMZ -----> ",x)
    print("-------------------------")

    input("\n\nPress Enter to continue...")
    print("\n-------------------------------------")




def get_list_from_ip_range(ip_range):

    ret = []

    #oredjivanje fiksong djela ip adrese
    counter = 0
    fixed_part = ""
    for i in range(len(ip_range)):
        if (ip_range[i]=="."):
            counter+=1
            if (counter == 3):
                fixed_part = ip_range[:i+1]
                break


    #odredjivanje ne fiksnog dijela adresa  
    temp1 = ip_range.split(".")[-1]
    num1 = int( temp1.split("-")[0] )
    num2 = int( temp1.split("-")[1] )

    # print(fixed_part)
    # print(num1)
    # print(num2)
    

    for x in range(num1, num2+1):
        ret.append(fixed_part+str(x))
    
    return ret


def print_infected():


    if ( len(infected_computers.keys()) == 0):
        print("\n\n ~ ~ no more computers were infected ~ ~")
    else:
        print("\n\n\ninfected computers >> ")
        for ip in infected_computers:
            # print("                     INFECTED.....", ip)
            inf_by = infected_computers.get(ip)[0]
            inf_time = infected_computers.get(ip)[1]
            if(inf_by == "1st infected "):
                print("\t\tINFECTED  .... ",ip, (15-len(ip))*" ","was first infected",(15-len(inf_by)+12)*" ", "at ", inf_time )
            else:
                print("\t\tINFECTED  .... ",ip, (15-len(ip))*" ","got infected by ",inf_by,(15-len(inf_by))*" ", "at ", inf_time )

        print()


def log_data():

    global start_time, end_time

    log_time_date = datetime.datetime.now().isoformat(' ', 'seconds')
    file_name = "log_ " + log_time_date.replace(":","-").replace(" ","--") + ".txt"

    dir_path = os.getcwd() 
    if (not os.path.exists("logs")):
        os.makedirs("logs")

    file_path = os.path.join(dir_path,"logs",file_name)
    file = open(file_path, "w+")
    
    file.write("\nlog report from " + str(log_time_date)+"\n"*2)
    for ip in infected_computers:
        inf_by = infected_computers.get(ip)[0]
        inf_time = infected_computers.get(ip)[1]
        file.write(" \t" + ip + (15-len(ip))*" " + "got infected by " + inf_by + (15-len(inf_by))*" " + "at " + str(inf_time) + "\n" )

    file.write("\n >>> Total time elapsed: "+ str( round(end_time-start_time,2)) + "s.\n")
    file.write(" >>> Number of propagation waves: " + str(propagation_wave_counter) + "\n") # 1: -->1st  2: 1st-->x1 ... 
    arr = []
    for k in infected_computers.values():
        arr.append(k[0])
    
    if(len(arr)>1):
        mc = collections.Counter(arr).most_common(1)[0]
        file.write(" >>> Computer " + str( mc[0]) +  " infected the most computers: " + str(mc[1]) + "\n\n")
    

    file.write("-"*160)

    file.close()




def crypt(list):
    global killswitch_domain_registered, killswitch_URL, check_real_domain,ksw_ip,ksw_time
    spread_to = []

    for var in list:
        infected = var[0] 
        infector = var[1]


        if ((ksw_time!=None and (time.time() - start_time)>=ksw_time and killswitch_domain_registered == False)):
            killswitch_domain_registered = True
            print("\n - - - killswitch domain registered ", ksw_time, "seconds after start - - -")
            time.sleep(4)
        if (ksw_ip != None and (ksw_ip in infected_computers.keys()) and killswitch_domain_registered == False):
            killswitch_domain_registered = True
            print("\n - - - killswitch domain registered after infection of ", ksw_ip, " - - -")
            time.sleep(4)
        
        if (check_real_domain==True and ping(killswitch_URL)==True):
            killswitch_domain_registered = True


        if(killswitch_domain_registered == False):
            infected_computers.update({infected:[infector,datetime.datetime.now()]}) #u ovom trenutku su podatci kriptirani
            print(infected, ".....................INFECTED..................... ")

            spread_to.extend(propagate(infected))


            #funkcije specificne za zadani zadatak, hardkodirane adrese itd..
            #umijesto toga napravljena funkcija propagate koja je primjenjiva za svaki teren opisan u network_landscape.jsoin i firewall_configuration.json

            # if(infected in datacenter_computers):
            #     spread_to.extend(propagate_from_datacenter(infected))

            # if(infected in DMZ_computers):
            #     spread_to.extend(propagate_from_DMZ(infected))

            # if(infected in WAN_computers):
            #     spread_to.extend(propagate_from_WAN(infected))
                
            # if(infected in distribution_computers):
            #     spread_to.extend(propagate_from_distribution(infected)) 
            


    if(spread_to == []):
        global end_time
        end_time = time.time()
        return
    else:
        global propagation_wave_counter
        print("\n\ncomputers infected from wave number ", propagation_wave_counter, " are: \n")
        propagation_wave_counter +=1
        for x in spread_to:
            print("     >> ", x[0])
        time.sleep(4)
        crypt(spread_to)
        



def propagate(infected_ip):
    #TO DO

    arr_2d = [] # [0]=infected, [1]=infector
    
    #izvuci podatke kuda se moze siriti iz jsona
    for interface in firewall["interfaces"]:
        if(is_ip_in_interface(infected_ip,interface["interface_ip"]) == True):
            blacklisted_ips = interface["blacklisted_ips"]
            interface_ip = interface["interface_ip"]
            available_interfaces = interface["available_interfaces"]
    

    #iteriraj po svim mogucim adresama i zarazi sto mozes
    for guessed_ip in all_possible_hosts:
        print("testing..... ", guessed_ip)
        time.sleep(0.001)

        reachable = False
        for av_if in available_interfaces:
            if(is_ip_in_interface(guessed_ip,av_if)):
                reachable = True

        #ako postoji pogadana ip adresa u lokalnoj mrezi i racunalo je ranjivo na EternalBlue
        vulnerable = False
        for computer in landscape["computers"]:
            if(computer["ip_address"] == guessed_ip and ("smb" in computer["protocols"] and ("Windows" in computer["os"]))):
                vulnerable = True

        #zarazi racunalo
        if(reachable == True and (guessed_ip not in blacklisted_ips) and (vulnerable == True)):
            arr_2d.append([guessed_ip,infected_ip])

        
    return arr_2d



def propagate_from_distribution(infected_ip):

    arr_2d = [] # [0]=infected, [1]=infector
    for computer in all_possible_hosts:     #pogadja ip adrese na temelju adrese sucelja
        
        print("testing..... ", computer)
        time.sleep(0.0005)

        #Lokalna mreza --> Internet
        #samo http i https
          
        # Lokalna mreza --> Datacenter
        if(computer in datacenter_computers):
            for x in landscape["computers"]:
                if(x["ip_address"] not in infected_computers and x["ip_address"]==computer and "smb" in  x["protocols"] and "Windows" in x["os"]):
                    if(x["description"]=="Database server" or x["description"]=="Private web server"):
                        arr_2d.append([x["ip_address"],infected_ip])        

        #Lokalna mreza --> WAN
        #Remote Desktop protokol
            #---> mimikatz

        admin = False
        if("192.168.53" in infected_ip):
            admin = True
        # Lokalna mreza --> DMZ
        if(computer in DMZ_computers):
            for x in landscape["computers"]:
                if(x["ip_address"] not in infected_computers and x["ip_address"]==computer and x["description"]=="Mail server" or x["description"]=="DNS server"):
                    if("smb" in  x["protocols"] and "Windows" in x["os"]):
                        arr_2d.append([x["ip_address"],infected_ip])
                if(admin and x["ip_address"] not in infected_computers and x["ip_address"]==computer and x["description"]=="Public web server"):
                    arr_2d.append([x["ip_address"],infected_ip])

        
    return arr_2d




#TO_DO
#ne moze nikuda iz DMZ
def propagate_from_DMZ(infected_ip):



    #iz DMZ-a NE MOZE do Lokalne mreze, to je i poanta!
    arr_2d =  []

    #iako nemoze nigdje iz DMZ-a ostavio sam petlju 
    #da simulira vrijeme koje potrosi probavajuci ip adrese
    for computer in all_possible_hosts:
        print("testing..... ", computer)
        time.sleep(0.0005)
        
    return arr_2d


    



def propagate_from_WAN(infected_ip):

    arr_2d = [] # [0]=infected, [1]=infector
   
    for computer in all_possible_hosts: #pogadja ip adrese na temelju adrese sucelja
        
        print("testing..... ", computer)
        time.sleep(0.0005)
          
        # WAN --> Datacenter
        if(computer in datacenter_computers):
            for x in landscape["computers"]:
                if(x["ip_address"] not in infected_computers and x["ip_address"]==computer and "smb" in  x["protocols"] and "Windows" in x["os"]):
                    if(x["description"]=="Database server" or x["description"]=="Private web server"):
                        arr_2d.append([x["ip_address"],infected_ip])        

        # WAN --> DMZ
        if(computer in DMZ_computers):
            for x in landscape["computers"]:
                if(x["ip_address"] not in infected_computers and  x["description"]=="Mail server"  or x["description"]=="DNS server"):
                    if("smb" in  x["protocols"] and "Windows" in x["os"]):
                        arr_2d.append([x["ip_address"],infected_ip])
        
    return arr_2d





def propagate_from_datacenter(infected_ip):

    arr_2d = []

    if(infected_ip == "192.168.52.100"): #Backup server
        
        for computer in all_possible_hosts:  #pogadja ip adrese na temelju adrese sucelja
            
            print("testing..... ", computer)
            time.sleep(0.0005) 

            # datacenter(backup server) --> DMZ
            if(computer in DMZ_computers):
                for x in landscape["computers"]:
                    if(x["ip_address"] not in infected_computers and x["ip_address"]==computer and x["description"]=="Backup server" and "smb" in  x["protocols"] and "Windows" in x["os"]):
                        arr_2d.append([x["ip_address"],infected_ip])
        
    return arr_2d




def print_start_message():
    f = open('startup_msg','r')
    print(f.read())


def main_menu():
    choice = 0;
    while(choice not in ['1','2','3','4']):
        print()
        print("1: Display network landscape info")
        print("2: Start worm propagation")
        print("3: Killswitch domain options")
        print("4: Exit")

        choice = input( ">> ")

    return int(choice)

def killswitch_menu():
    
    global ksw_ip
    global ksw_time

    choice = 0
    while(choice not in [1,2,3]):
        print()
        print("1: Trigger killswitch after some amount of time")
        print("2: Trigger killswitch after specific computer gets infected")
        print("3: Use real domain: www.iuqerfsodp9ifjaposdfjhgosurijfaewrwergwea.com ")
        print("4: For UNIX like machines killswitch can be registered with SIGQUIT (Ctrl + \)")

        choice = int(input(">> "))

    if (choice == 1):
        flag = True
        while( flag ):
                ksw_time = input("enter time in seconds >> ")
                if( not ksw_time.isdigit()):
                    print("Time must be an integer greater or equal to zero")
                else:
                    flag = False
                    ksw_time = int(ksw_time)
                    print("killswitch domain will be registered ", ksw_time, " seconds after the worm propagation starts")
    

    elif (choice == 2):
        flag = True 

        while( flag ):
            ksw_ip = input("enter computers ip >> ")
            if (ksw_ip in computers):
                print("killswitch domain will be registered after ", ksw_ip, " gets infected")
                flag = False
            else:
                print("There is no computer with provided ip addres.")
    
    elif(choice == 3):
        global check_real_domain
        check_real_domain = True



def print_stats():
    
    global start_time
    global end_time
    print("\n >>> Total time elapsed: ", round(end_time-start_time,2),"s.")

    print(" >>> Number of propagation waves: ",propagation_wave_counter) # 1: -->1st  2: 1st-->x1 ... 

    arr = []
    for k in infected_computers.values():
        arr.append(k[0])
    
    if(len(arr)>1):
        mc = collections.Counter(arr).most_common(1)[0]
        print(" >>> Computer ", mc[0], " infected the most computers: ", mc[1])

        for k in landscape["computers"]:
            if('-' in k["ip_address"]):
                if(mc[0] in get_list_from_ip_range(k["ip_address"])):
                    print("\tcomputer description: ",k["description"])
                    print("\tcomputer os :",k["os"] if k["os"]!="" else "unknown")
                    print("\trunning applications :",k["applications"] if k["applications"]!="" else "unknown")
            elif(k["ip_address"] == str(mc[0])):
                print("\tcomputer description: ",k["description"])
                print("\tcomputer os :",k["os"])
                print("\trunning applications :",k["applications"] if k["applications"]!="" else "unknown")





def main():

    #handlanje signala za killswitch domenu
    signal.signal(signal.SIGQUIT, SIGQUIT_handler)

    print_start_message()
    initialize_global_variables()
    global start_time
    global end_time

    while(True):
        choice = main_menu()
        if(choice == 1):
            print_landscape_info()
            print_bank_info()
        elif(choice == 2):
            flag = False
            while(flag == False):
                first_infected = input("input the infected computers ip address > ")
                time.sleep(2)
                if(first_infected not in computers):
                    print("There is no computer with provided ip addres.\nPlease try again")
                else:
                    start_time = time.time()
                    flag = True
                    print("worm propagation: ")
                    time.sleep(2)
                    crypt([[first_infected, "1st infected "]])
            print_infected()
            print_stats()
            log_data()
            reset_global_variables()
        elif(choice == 3):
            killswitch_menu()
        elif(choice == 4):
            sys.exit()
    




main()


